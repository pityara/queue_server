#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
char msg[] = "Hi";
char byemsg[] = "bye";

int main(){
  char buf[1024];
  int bytes_read;
  int sock;
  struct sockaddr_in addr;
  sock = socket(AF_INET, SOCK_DGRAM, 0);
  if(sock < 0) {
        perror("socket");
        exit(1); }
  addr.sin_family = AF_INET;
  addr.sin_port = htons(24567);
  addr.sin_addr.s_addr = inet_addr("10.211.55.21");
  sendto(sock, msg, sizeof(msg), 0, (struct sockaddr *) &addr, sizeof(addr));
  int ssock;
  struct sockaddr_in in_addr;
  in_addr.sin_family = AF_INET;
  in_addr.sin_port = htons(24567);
  ssock = socket(AF_INET, SOCK_DGRAM, 0);
  in_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  if(bind(ssock, (struct sockaddr *)&in_addr, sizeof(in_addr)) < 0) {
    perror("bind");
    exit(2);
  }
  while(1) {
    bytes_read = recvfrom(ssock, buf, 1024, 0, NULL, NULL);
    if(strcmp(buf, byemsg) == 0) break;
    buf[bytes_read] = '\0';
    printf(buf);
    printf("\n");
    fflush(stdout);
  }
  close(sock);
  return 0;

}
