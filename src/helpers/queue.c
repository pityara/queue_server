#include "queue.h"

Queue* push(Queue* queue, char addr[], int n) {
  Client* client = malloc(sizeof(Client));
  strcpy(client->addr, addr);
  client->number = n;
  client->next = NULL;
  if(queue->head && queue->tail) {
    queue->tail->next = client;
    queue->tail = client;
  } else {
    queue->head = queue->tail = client;
  }
  return queue;
}

char* pop(Queue* queue) {
  char* addr;
  if(queue->head){
    addr = queue->head->addr;
    queue->head = queue->head->next;
  }
  return addr;
}
