#include "queue.h"
int send_dgramto(char* ip, char *msg, int len){
  SOCKET msock;
  struct sockaddr_in maddr;
  msock = socket(AF_INET, SOCK_DGRAM, 0);
  maddr.sin_family = AF_INET;
  maddr.sin_port = htons(24567);
  maddr.sin_addr.s_addr = inet_addr(ip);
  return sendto(msock, msg, len, 0, (struct sockaddr *) &maddr, sizeof(maddr));
}
