#include "winsock2.h"
#include <unistd.h>
#include <stdio.h>
#include <Windows.h>
#include <string.h>
typedef struct _Client{

  struct _Client* next;
  char addr[20];
  int number;
} Client;

typedef struct _Queue{
  Client* head;
  Client* tail;
} Queue;

Queue* push(Queue* queue, char* addr, int n);
char* pop(Queue* queue);
int send_dgramto(char* ip, char *msg, int len);
