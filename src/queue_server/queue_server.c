#include "queue.h"

typedef DWORD(*WinThread) (LPVOID a);
Queue queue = { NULL, NULL };
DWORD ThreadID;
HANDLE mutex;
HANDLE semA;
HANDLE semB[2];
char buf[1024];
HANDLE workerThread;


WinThread worker(LPVOID ptr) {
  char* addr = (char*) ptr;
  char begining[] = "Start doing some job...\n";
  char ending[] = "Finish doing some job... \n";
  char byemsg[] = "bye";
  int status;
  if (WaitForSingleObject(semA, 0L) == WAIT_OBJECT_0) {
    printf("Some work for client with ip: %s try to start\n", addr);
    status = send_dgramto(addr, begining, sizeof(begining));
    if (status == -1 ){
      printf("Some problems with sending message#1!\n");
      exit(6);
    }
    Sleep(5000);
    status = send_dgramto(addr, ending, sizeof(ending));
    if (status == -1) {
      printf("Some problems with sending message #2!\n");
      exit(7);
    }
    send_dgramto(addr, byemsg, sizeof(byemsg));
    ReleaseSemaphore(semB[0], 1, NULL);
    printf("Session for client with ip: %s is ended, runing notifier...\n", addr);
  }
}

WinThread queueCreator(LPVOID ptr) {
  SOCKET* sock = (SOCKET *) ptr;
  SOCKET s;
  struct sockaddr_in from;
  int len = sizeof(from);
  int nu;
  char* ip;
  char ms[100];
  s = socket(AF_INET, SOCK_DGRAM, 0);
  while(1){
    recvfrom(*sock, buf, 1024, 0, (struct sockaddr *)&from, &len);
    ip = inet_ntoa(from.sin_addr);
    if(queue.tail!=NULL) {
      nu = queue.tail->number + 1;
      snprintf(ms, 100, "Sorry, server is busy you are placed to queue with number: %d", nu);
      send_dgramto(ip, ms, sizeof(ms));
    } else {
      nu = 0;
    }
    printf("Some task from client with ip: %s pushed to queue, current queue length: %d \n", ip, nu);
    push(&queue, ip, nu);
    ReleaseSemaphore(semB[1], 1, NULL);
  }
}
WinThread notifier(LPVOID ptr) {
  Client* current;
  SOCKET notify_sock;
  struct sockaddr_in notify_addr;
  notify_sock = socket(AF_INET, SOCK_DGRAM, 0);
  char* ip;
  while(1){
    DWORD res = WaitForMultipleObjects(2, semB, TRUE, 0L);
      if (res >= WAIT_OBJECT_0 && res < WAIT_OBJECT_0 + 2){
        printf("The queue is not empty, trying to run worker thread... \n");
        printf("Notify all clients and starting working thread...\n");
        workerThread = CreateThread(NULL,
                                    0,
                                    (LPTHREAD_START_ROUTINE) worker,
                                    (LPVOID) pop(&queue),
                                    0,
                                    &ThreadID
                                    );
        ReleaseSemaphore(semA, 1, NULL);
        current = queue.head;
        int i = 0;
        while(current){
          i = i+1;
          current->number = i;
          ip = current->addr;
          char msgQ[20];
          sprintf(msgQ, "You are %d in queue", i);
          send_dgramto(ip, msgQ, sizeof(msgQ));
          current = current->next;
        }
    }
  }
}





int main()
{
    WSADATA wsaData;
    int bytes_read;
    HANDLE queueCr, notifyWorker;
    semA = CreateSemaphore(NULL, 0, 12, NULL);
    semB[0] = CreateSemaphore(NULL, 1, 5, NULL);
    semB[1] = CreateSemaphore(NULL, 0, 5, NULL);
    int iResult = WSAStartup( MAKEWORD(2,2), &wsaData );
    if ( iResult != NO_ERROR )
        printf("Error at WSAStartup()\n");
    SOCKET m_socket;
    SOCKET sock;
    m_socket = socket( AF_INET, SOCK_DGRAM, 0 );

    if ( m_socket == INVALID_SOCKET )
    {
        printf( "Error at socket(): %ld\n", WSAGetLastError() );
        WSACleanup();
        return 1;
    }

    struct sockaddr_in service;
    service.sin_family = AF_INET;
    service.sin_addr.s_addr = inet_addr("192.168.56.1");
    service.sin_port = htons( 24567 );

    if ( bind( m_socket, (SOCKADDR*) &service, sizeof(service) ) == SOCKET_ERROR )
    {
        printf( "bind() failed.\n" );
        closesocket(m_socket);
        return 1;
    }
    queueCr = CreateThread(NULL,
                           0,
                           (LPTHREAD_START_ROUTINE) queueCreator,
                           (LPVOID) &m_socket,
                           0,
                           &ThreadID);
    notifyWorker = CreateThread(NULL,
                                0,
                                (LPTHREAD_START_ROUTINE) notifier,
                                NULL,
                                0,
                                &ThreadID);
    WaitForSingleObject(queueCr, INFINITE);
    WaitForSingleObject(notifyWorker, INFINITE);
    CloseHandle(notifyWorker);
    CloseHandle(queueCr);
    return 0;
}
